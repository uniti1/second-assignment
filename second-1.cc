#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>

using namespace std;

#include "ns3/boolean.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/double.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/log.h"
#include "ns3/mobility-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/uinteger.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"

#include <iomanip>

using namespace ns3;

// Global variables for use in callbacks.
double g_signalDbmAvg; //!< Average signal power [dBm]
double g_noiseDbmAvg;  //!< Average noise power [dBm]
uint32_t g_samples;    //!< Number of samples

/**
 * Monitor sniffer Rx trace
 *
 * \param packet The sensed packet.
 * \param channelFreqMhz The channel frequency [MHz].
 * \param txVector The Tx vector.
 * \param aMpdu The aMPDU.
 * \param signalNoise The signal and noise dBm.
 * \param staId The STA ID.
 */
void
MonitorSniffRx(Ptr<const Packet> packet,
               uint16_t channelFreqMhz,
               WifiTxVector txVector,
               MpduInfo aMpdu,
               SignalNoiseDbm signalNoise,
               uint16_t staId)

{
    g_samples++;
    g_signalDbmAvg += ((signalNoise.signal - g_signalDbmAvg) / g_samples);
    g_noiseDbmAvg += ((signalNoise.noise - g_noiseDbmAvg) / g_samples);
}

NS_LOG_COMPONENT_DEFINE("WifiSpectrumPerExample");

int
main(int argc, char* argv[])
{
    double distance = 50;
    double simulationTime = 10; // seconds
    std::string wifiType = "ns3::YansWifiPhy";
    std::string errorModelType = "ns3::NistErrorRateModel";
    bool enablePcap = true;
         clock_t clock1, clock2;


    CommandLine cmd(__FILE__);
    cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
    cmd.AddValue("distance", "meters separation between nodes", distance);
    cmd.AddValue("wifiType", "select ns3::SpectrumWifiPhy or ns3::YansWifiPhy", wifiType);
    cmd.AddValue("errorModelType",
                 "select ns3::NistErrorRateModel or ns3::YansErrorRateModel",
                 errorModelType);
    cmd.AddValue("enablePcap", "enable pcap output", enablePcap);
    cmd.Parse(argc, argv);

  
        uint32_t    payloadSize = 1448; // 1500 bytes IPv6
            Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));
  

        NodeContainer wifiStaNode;
        wifiStaNode.Create(1);
        NodeContainer wifiApNode;
        wifiApNode.Create(1);

        YansWifiPhyHelper phy;
        SpectrumWifiPhyHelper spectrumPhy;
        if (wifiType == "ns3::YansWifiPhy")
        {
            YansWifiChannelHelper channel;
            channel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                       "Frequency",
                                       DoubleValue(5.180e9));
            channel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
            phy.SetChannel(channel.Create());
            phy.Set("TxPowerStart", DoubleValue(1)); // dBm (1.26 mW)
            phy.Set("TxPowerEnd", DoubleValue(1));
        }
        else if (wifiType == "ns3::SpectrumWifiPhy")
        {
            Ptr<MultiModelSpectrumChannel> spectrumChannel =
                CreateObject<MultiModelSpectrumChannel>();
            Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
            lossModel->SetFrequency(5.180e9);
            spectrumChannel->AddPropagationLossModel(lossModel);

            Ptr<ConstantSpeedPropagationDelayModel> delayModel =
                CreateObject<ConstantSpeedPropagationDelayModel>();
            spectrumChannel->SetPropagationDelayModel(delayModel);

            spectrumPhy.SetChannel(spectrumChannel);
            spectrumPhy.SetErrorRateModel(errorModelType);
            spectrumPhy.Set("TxPowerStart", DoubleValue(1)); // dBm  (1.26 mW)
            spectrumPhy.Set("TxPowerEnd", DoubleValue(1));
        }
        else
        {
            NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
        }

        WifiHelper wifi;
        wifi.SetStandard(WIFI_STANDARD_80211n);
        WifiMacHelper mac;

        Ssid ssid = Ssid("ns380211n");

        StringValue DataRate = StringValue("HtMcs0");

        wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                     "DataMode",
                                     DataRate,
                                     "ControlMode",
                                     DataRate);

        NetDeviceContainer staDevice;
        NetDeviceContainer apDevice;

        if (wifiType == "ns3::YansWifiPhy")
        {
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            phy.Set("ChannelSettings",
                    StringValue(std::string("{40, 0, BAND_5GHZ, 0}")));
            staDevice = wifi.Install(phy, mac, wifiStaNode);
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
            apDevice = wifi.Install(phy, mac, wifiApNode);
        }
        else if (wifiType == "ns3::SpectrumWifiPhy")
        {
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            phy.Set("ChannelSettings",
                    StringValue(std::string("{40, 0, BAND_5GHZ, 0}")));
            staDevice = wifi.Install(spectrumPhy, mac, wifiStaNode);
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
            apDevice = wifi.Install(spectrumPhy, mac, wifiApNode);
        }

        // mobility.
        MobilityHelper mobility;
        Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

        positionAlloc->Add(Vector(0.0, 0.0, 0.0));
        positionAlloc->Add(Vector(distance, 0.0, 0.0));
        mobility.SetPositionAllocator(positionAlloc);

        mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

        mobility.Install(wifiApNode);
        mobility.Install(wifiStaNode);

        /* Internet stack*/
        InternetStackHelper stack;
        stack.Install(wifiApNode);
        stack.Install(wifiStaNode);

        Ipv4AddressHelper address;
        address.SetBase("192.168.1.0", "255.255.255.0");
        Ipv4InterfaceContainer staNodeInterface;
        Ipv4InterfaceContainer apNodeInterface;

        staNodeInterface = address.Assign(staDevice);
        apNodeInterface = address.Assign(apDevice);

        /* Setting applications */
        ApplicationContainer serverApp;

        // TCP flow
        uint16_t port = 50000;
        Address localAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
        PacketSinkHelper packetSinkHelper("ns3::TcpSocketFactory", localAddress);
        serverApp = packetSinkHelper.Install(wifiStaNode.Get(0));
        serverApp.Start(Seconds(0.0));
        serverApp.Stop(Seconds(simulationTime + 1));

        OnOffHelper onoff("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress(InetSocketAddress(staNodeInterface.GetAddress(0), port));
        onoff.SetAttribute("Remote", remoteAddress);
        ApplicationContainer clientApp = onoff.Install(wifiApNode.Get(0));
        clientApp.Start(Seconds(1.0));
        clientApp.Stop(Seconds(simulationTime + 1));

        if (enablePcap)
        {
            phy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            std::stringstream ss;
            phy.EnablePcap("ap", apDevice);
            phy.EnablePcap("sta", staDevice);
        }
        g_signalDbmAvg = 0;
        g_noiseDbmAvg = 0;
        g_samples = 0;

        Simulator::Stop(Seconds(simulationTime + 1));
        clock1 = clock();

        Simulator::Run();

        Simulator::Destroy();
        clock2 = clock();

        cout << "Execution time " << (float)(clock2 - clock1) / CLOCKS_PER_SEC << " " << endl;
        ;

        return 0;
    }
